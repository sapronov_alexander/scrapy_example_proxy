# coding: utf-8

from django.contrib import admin
from django.contrib.auth.admin import Group as BaseGroup
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _
from models import Group, User


class UserAdmin(BaseUserAdmin):
    fieldsets = ((None, {'fields': ('username', 'password')}),
                 (_('Personal info'), {
                     'fields':
                     ('first_name', 'last_name', 'email', 'number_proxy')
                 }),
                 (_('Permissions'), {'fields':
                                     ('is_active', 'is_staff', 'is_superuser',
                                      'groups', 'user_permissions')}),
                 (_('Important dates'), {'fields':
                                         ('last_login', 'date_joined')}), )
    add_fieldsets = ((None, {
        'classes': ('wide', ),
        'fields': ('username', 'password1', 'password2', 'number_proxy')
    }), )


class GroupAdmin(BaseGroupAdmin):
    list_display = ('name', 'number_proxy', )


admin.site.register(User, UserAdmin)
admin.site.unregister(BaseGroup)
admin.site.register(Group, GroupAdmin)
