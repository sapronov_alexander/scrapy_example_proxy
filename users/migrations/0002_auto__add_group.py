# -*- coding: utf-8 -*-
from django.db import models
from south.db import db
from south.utils import datetime_utils as datetime
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Adding model 'Group'
        db.create_table(
            u'users_group',
            ((u'group_ptr',
              self.gf('django.db.models.fields.related.OneToOneField')(
                  to=orm['auth.Group'],
                  unique=True,
                  primary_key=True)),
             ('number_proxy', self.gf('django.db.models.fields.IntegerField')(
                 default=10)), ))
        db.send_create_signal(u'users', ['Group'])

    def backwards(self, orm):
        # Deleting model 'Group'
        db.delete_table(u'users_group')

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name':
            ('django.db.models.fields.CharField', [], {'unique': 'True',
                                                       'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField',
                            [], {'to': u"orm['auth.Permission']",
                                 'symmetrical': 'False',
                                 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta':
            {'ordering':
             "(u'content_type__app_label', u'content_type__model', u'codename')",
             'unique_together': "((u'content_type', u'codename'),)",
             'object_name': 'Permission'},
            'codename':
            ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [],
                             {'to': u"orm['contenttypes.ContentType']"}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name':
            ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)",
                     'unique_together': "(('app_label', 'model'),)",
                     'object_name': 'ContentType',
                     'db_table': "'django_content_type'"},
            'app_label':
            ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model':
            ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name':
            ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.group': {
            'Meta': {'object_name': 'Group',
                     '_ormbases': [u'auth.Group']},
            u'group_ptr': ('django.db.models.fields.related.OneToOneField', [],
                           {'to': u"orm['auth.Group']",
                            'unique': 'True',
                            'primary_key': 'True'}),
            'number_proxy':
            ('django.db.models.fields.IntegerField', [], {'default': '10'})
        },
        u'users.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [],
                            {'default': 'datetime.datetime.now'}),
            'email':
            ('django.db.models.fields.EmailField', [], {'max_length': '75',
                                                        'blank': 'True'}),
            'first_name':
            ('django.db.models.fields.CharField', [], {'max_length': '30',
                                                       'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [],
                       {'symmetrical': 'False',
                        'related_name': "u'user_set'",
                        'blank': 'True',
                        'to': u"orm['auth.Group']"}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active':
            ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff':
            ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser':
            ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [],
                           {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [],
                          {'max_length': '30',
                           'blank': 'True'}),
            'number_proxy': ('django.db.models.fields.IntegerField', [],
                             {'default': '0'}),
            'password': ('django.db.models.fields.CharField', [],
                         {'max_length': '128'}),
            'user_permissions': (
                'django.db.models.fields.related.ManyToManyField', [],
                {'symmetrical': 'False',
                 'related_name': "u'user_set'",
                 'blank': 'True',
                 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [],
                         {'unique': 'True',
                          'max_length': '30'})
        }
    }

    complete_apps = ['users']
