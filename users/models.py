# coding: utf-8

from django.contrib.auth.models import AbstractUser as BaseUser
from django.contrib.auth.models import Group, GroupManager
from django.db import models
from django.utils.translation import ugettext_lazy as _


class User(BaseUser):
    number_proxy = models.IntegerField(verbose_name=_(u'Количество проксей'),
                                       default=0)


class Group(Group):
    number_proxy = models.IntegerField(verbose_name=_(u'Количество проксей'),
                                       default=10)

    objects = GroupManager()
