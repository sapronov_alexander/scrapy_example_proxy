from api.views import ProxyListView, ProxyViewSet
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'proxy/$', ProxyViewSet.as_view({'get': 'get_proxy'})),
    url(r'proxy/list$', ProxyListView.as_view()),
    url(r'proxy/country/(?P<cn>\w+)/$',
        ProxyViewSet.as_view({'get': 'get_proxy_with_country'})), )
