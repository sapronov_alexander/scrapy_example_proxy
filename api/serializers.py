# coding: utf-8

from backend.models import Proxy
from rest_framework import serializers


class ProxySerializer(serializers.ModelSerializer):
    class Meta:
        model = Proxy
        fields = ('id', 'ip', 'port', 'country', 'url')
