# coding: utf-8

from django.http import Http404
from rest_framework import generics, viewsets
from rest_framework.response import Response

from backend.models import Proxy
from serializers import ProxySerializer
from users.models import Group


class ProxyListView(generics.ListAPIView):
    serializer_class = ProxySerializer
    queryset = Proxy.objects.filter(type='HTTP')
    paginate_by = 100


class ProxyViewSet(viewsets.ModelViewSet):
    model = Proxy

    def get_proxy(self, *args, **kwargs):
        print('get_proxy')
        number_proxy = self.get_number_proxy(self.request)
        print(type(Proxy.get_proxy(number_proxy)))
        serializer = ProxySerializer(Proxy.get_proxy(number_proxy), many=True)
        return Response(serializer.data)

    def get_proxy_with_country(self, *args, **kwargs):
        print('get_proxy_with_country')
        number_proxy = self.get_number_proxy(self.request)
        serializer = ProxySerializer(Proxy.get_proxy_with_country(
                number_proxy, kwargs['cn']), many=True)
        return Response(serializer.data)

    def get_number_proxy(self, request):
        if not request.user.is_authenticated:
            raise Http404
        if request.user.number_proxy:
            return request.user.number_proxy
        groups = request.user.groups.all()
        if not groups:
            raise Http404

        # TODO попробовать получить number_proxy проще
        l = []
        for group in groups:
            try:
                gr = Group.objects.get(id=group.pk)
            except Group.DoesNotExist:
                continue
            l.append(gr.number_proxy)
        number_proxy = max(l)
        return number_proxy
