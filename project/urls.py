from django.conf.urls import include, patterns, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'', include('frontend.urls')),
                       url(r'^api/', include('api.urls')),
                       url(r'^docs/', include('rest_framework_swagger.urls')),
                       url(r'^grappelli/', include('grappelli.urls')),
                       url(r'^admin/', include(admin.site.urls)), )
