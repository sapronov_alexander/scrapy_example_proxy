# coding: utf-8

import os

import djcelery
from django.core.urlresolvers import reverse_lazy

djcelery.setup_loader()
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = ')&^7x8%e%!!x&ex#p#gds3nbv6a%$r__hsji%$wk38$hrph4*^'

DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = []

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder', )

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages', )

TEMPLATE_LOADERS = ('django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                    'apptemplates.Loader', )

# Application definition

INSTALLED_APPS = (
    # interface
    'grappelli_extensions',
    'grappelli.dashboard',
    'grappelli',
    'daterange_filter',

    # django
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'south',
    'djcelery',
    'djkombu',
    'dynamic_scraper',
    'scrapy_djangoitem',
    'rest_framework',
    'rest_framework_swagger',

    #################
    'backend',
    'users',
    'frontend', )

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware', )

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend', )

SITE_ID = 1

GRAPPELLI_EXTENSIONS_NAVBAR = 'project.extensions.Navbar'
# GRAPPELLI_EXTENSIONS_SIDEBAR = 'project.extensions.Sidebar'
GRAPPELLI_INDEX_DASHBOARD = 'project.dashboard.CustomIndexDashboard'
#

ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'

LANGUAGE_CODE = 'ru-Ru'
TIME_ZONE = 'Asia/Novosibirsk'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

AUTH_USER_MODEL = 'users.User'

DEBUG_TOOLBAR_PATCH_SETTINGS = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite'),
    }
}

BROKER_BACKEND = 'djkombu.transport.DatabaseTransport'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

CELERY_TIMEZONE = 'Asia/Novosibirsk'

REST_FRAMEWORK = {
    'DEFAULT_MODEL_SERIALIZER_CLASS':
    'rest_framework.serializers.HyperlinkedModelSerializer',
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
DEFAULT_FROM_EMAIL = 'proxyrepository@gmail.com'
EMAIL_HOST_USER = 'proxyrepository@gmail.com'
EMAIL_HOST_PASSWORD = 'proxybase3'
EMAIL_PORT = 587

try:
    from local_settings import *
except ImportError:
    pass
