# -*- encoding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from grappelli.dashboard import Dashboard, modules
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """Custom index dashboard for www."""

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        # append an app list module for "Applications"
        self.children.append(modules.AppList(
            _(u'Applications'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed', ),
            exclude=('django.contrib.*', ), ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _(u'Recent Actions'),
            limit=5,
            collapsible=False,
            column=2, ))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _(u'Support'),
            column=3,
            children=[
                {
                    'title': _('Django Documentation'),
                    'url': 'http://docs.djangoproject.com/',
                    'external': True,
                },
                {
                    'title': _('Grappelli Documentation'),
                    'url': 'http://packages.python.org/django-grappelli/',
                    'external': True,
                },
                {
                    'title': _('Grappelli Google-Code'),
                    'url': 'http://code.google.com/p/django-grappelli/',
                    'external': True,
                },
            ]))
