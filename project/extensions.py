# -*- encoding: utf-8 -*-

from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from grappelli_extensions.nodes import CLNode


class Navbar(object):
    nodes = (CLNode('backend', 'proxy', _(u"Прокси")),
             CLNode('backend', 'taskscheduler', _(u"Расписание")), )

#
# class Sidebar(object):
#     nodes = (
#         ('Auth', {'nodes': (
#             ('Users', {
#                 'url': reverse_lazy('admin:auth_user_changelist'),
#                 'perm': 'auth.change_user',
#             }),
#             ('Groups', {
#                 'url': reverse_lazy('admin:auth_group_changelist'),
#                 'perm': 'auth.change_group',
#             }),
#         )}),
#     )
