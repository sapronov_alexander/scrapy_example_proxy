### Настройка виртуального окружения
*    создать виртуальное окружение:
     virtualenv --no-site-package

*    активировать вирт. окружение
     source venv/bin/activate    
    
*    деактивировать вирт. окружение
     deactivate
    

### Встраивание паука
*   добавить паука в каталог interface/src/backend/scrapers/scrapycustom/scrapycustom/spiders
    название модуля паука должно совпадать с name в классе паука 

*   добавить таску для паука в interface/src/backend/tasks.py
    наследоваться от класса BaseTask
    указать атрибуты : spider_name - название паука, site_url - сайт который будет парсится
    

### Расписание 
*   Зайти в админку, в "Расписание", выбрать таску, указать дату и время для запуска таски
    Если флаг repeat==False, то таска выполнится только один раз
    

### Запуск celery
*   source venv/bin/activate
*   cd interface/src
*   python manage.py celeryd -l info -B -E