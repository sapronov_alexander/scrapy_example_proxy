# encoding: utf-8

from base_task import BaseTask
u"""
Значаение spider_name должно совпадать с name в классе паука
"""


class FineproxyOrgSpider(BaseTask):
    spider_name = 'fineproxy_org'
    site_url = 'fineproxy.org'


class FoxtoolsRuSpider(BaseTask):
    spider_name = 'foxtools_ru'
    site_url = 'foxtools.ru'


class MultiproxyOrgSpider(BaseTask):
    spider_name = 'multiproxy_org'
    site_url = 'multiproxy.org'


class PrimeSpeedOrgSpider(BaseTask):
    spider_name = 'prime-speed_ru'
    site_url = 'prime-speed.ru'


class ProxylistOrgSpider(BaseTask):
    spider_name = 'proxy-list_org'
    site_url = 'proxy-list.org'


class ProxyHttpNetSpider(BaseTask):
    spider_name = 'proxyhttp_net'
    site_url = 'proxyhttp.net'


class ProxylifeOrgSpider(BaseTask):
    spider_name = 'proxylife_org'
    site_url = 'proxylife.org'


class SergeykovaletsComSpider(BaseTask):
    spider_name = 'sergeykovalets_com'
    site_url = 'sergeykovalets.com'


class StopinfectionNarodRuSpider(BaseTask):
    spider_name = 'stopinfection_narod_ru'
    site_url = 'stopinfection.narod.ru'


class TherealistRuSpider(BaseTask):
    spider_name = 'therealist_ru'
    site_url = 'therealist.ru'
