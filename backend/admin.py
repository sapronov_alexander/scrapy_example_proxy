from django.contrib import admin
from django.contrib.admin.widgets import AdminDateWidget
from djcelery.admin import PeriodicTaskAdmin
from djcelery.models import *
from models import Proxy
from models import Site
from models import TaskScheduler

# hide djcelery admin menu
# admin.site.unregister(IntervalSchedule)
# admin.site.unregister(CrontabSchedule)
# admin.site.unregister(PeriodicTask)
# admin.site.unregister(WorkerState)
# admin.site.unregister(TaskState)


class TaskSchedulerAdmin(PeriodicTaskAdmin):
    formfield_overrides = {models.DateTimeField: {'widget': AdminDateWidget}}
    model = TaskScheduler

    list_display = ('name',
                    'task',
                    'interval',
                    'crontab',
                    'repeat',
                    'enabled',
                    'last_run_at',
                    'total_run_count',
                    'date_changed', )

    search_fields = ('name', 'task', 'last_run_at', )

    list_filter = ('task',
                   'repeat',
                   'enabled',
                   'last_run_at',
                   'date_changed', )

    fields = ('name', 'regtask', 'task', 'interval', 'crontab', 'repeat')

    fieldsets = None


class ProxyAdmin(admin.ModelAdmin):
    formfield_overrides = {models.DateTimeField: {'widget': AdminDateWidget}}
    list_display = ('ip', 'port', 'country', 'type', 'site', 'last_update')
    list_display_links = ('ip', )

    list_filter = ('country', 'type', 'last_update', )

    search_fields = ('ip',
                     'port',
                     'country',
                     'login',
                     'password',
                     'type'
                     'site', )

    readonly_fields = ('url', )

# class SiteAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name', 'url', )
#     list_display_links = ('id', 'name', )

# admin.site.register(Site, SiteAdmin)
admin.site.register(Proxy, ProxyAdmin)
admin.site.register(TaskScheduler, TaskSchedulerAdmin)

#
# admin.site.register_app_label("backend", _(u"My awesome web app"))
