# encoding: utf-8

import json

from backend.tasks_proxy import CheckProxyTask
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djcelery.models import PeriodicTask
from dynamic_scraper.models import Scraper
from scrapy_djangoitem import DjangoItem

APP_NAME = _(u'Управление')


class string_with_title(str):
    def __new__(cls, value, title):
        instance = str.__new__(cls, value)
        instance._title = title
        return instance

    def title(self):
        return self._title

    __copy__ = lambda self: self
    __deepcopy__ = lambda self, memodict: self


class Site(models.Model):
    name = models.CharField(max_length=256, verbose_name=_(u'Название сайта'))
    url = models.URLField(verbose_name=_(u'Адрес'))

    scraper = models.ForeignKey(Scraper,
                                blank=True,
                                null=True,
                                on_delete=models.SET_NULL)

    # scraper_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.name

    class Meta:
        app_label = string_with_title('backend', APP_NAME)
        verbose_name = _(u'Сайт')
        verbose_name_plural = _(u'Сайты')


class Proxy(models.Model):
    ip = models.CharField(max_length=255, verbose_name=_(u'IP адрес'))

    port = models.CharField(max_length=255, verbose_name=_(u'Порт'))

    login = models.CharField(max_length=255,
                             verbose_name=_(u'Логин'),
                             blank=True,
                             null=True)

    password = models.CharField(max_length=255,
                                verbose_name=_(u'Пароль'),
                                blank=True,
                                null=True)

    country = models.CharField(max_length=255,
                               verbose_name=_(u'Страна'),
                               blank=True,
                               null=True)

    type = models.CharField(max_length=255,
                            verbose_name=_(u'Тип'),
                            blank=True,
                            null=True)

    uptime_percent = models.CharField(max_length=255, blank=True, null=True)

    last_update = models.CharField(max_length=255,
                                   verbose_name=_(u'Последнее обновление'),
                                   blank=True,
                                   null=True)

    url = models.URLField(verbose_name=_(u'Адрес'))

    site = models.ForeignKey(Site, blank=True, null=True)

    # checker_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.url

    class Meta:
        app_label = string_with_title('backend', APP_NAME)
        verbose_name = _(u'Прокси')
        verbose_name_plural = _(u'Прокси')

    def save(self,
             force_insert=False,
             force_update=False,
             using=None,
             update_fields=None):
        if not Proxy.objects.filter(ip=self.ip, port=self.port).exists():
            super(Proxy, self).save(force_insert, force_update, using,
                                    update_fields)
            CheckProxyTask().apply_async(args=(self.id, ))

    @staticmethod
    def get_proxy(n):
        # TODO использовать numpy.random.random_integers
        list_proxy = Proxy.objects.order_by('?')
        if not list_proxy:
            return []
        if list_proxy.count() < n:
            return list_proxy[:list_proxy.count()]
        return list_proxy[:n]

    @staticmethod
    def get_proxy_with_country(n, country):
        # TODO использовать numpy.random.random_integers
        list_proxy = Proxy.objects.filter(country=country).order_by('?')
        if not list_proxy:
            return []
        if list_proxy.count() < n:
            return list_proxy[:list_proxy.count()]
        return list_proxy[:n]


class ProxyItem(DjangoItem):
    django_model = Proxy


class TaskScheduler(PeriodicTask):
    repeat = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.kwargs = json.dumps({'name': self.name})
        super(TaskScheduler, self).save(*args, **kwargs)

    def stop(self):
        u"""Остановить таску"""
        self.enabled = False
        self.save()

    def start(self):
        u"""Запуск таски"""
        self.enabled = True
        self.save()

    def terminate(self):
        u"""Удаление таски"""
        self.stop()
        self.delete()

    class Meta:
        verbose_name = _(u'Расписание')
        verbose_name_plural = _(u'Расписания')
