# coding: utf-8

from random import choice

import requests
import requests.exceptions
import socks  # https://github.com/Anorov/PySocks
from celery.schedules import crontab
from celery.task import PeriodicTask, Task
from lxml import html
from requests import auth, head


def get_anon_keys():
    return [
        "HTTP_VIA",
        "HTTP_X_FORWARDED_FOR",
        "HTTP_FORWARDED_FOR",
        "HTTP_X_FORWARDED",
        "HTTP_FORWARDED",
        "HTTP_CLIENT_IP",
        "HTTP_FORWARDED_FOR_IP",
        "VIA",
        "X_FORWARDED_FOR",
        "FORWARDED_FOR",
        "X_FORWARDED",
        "FORWARDED",
        "CLIENT_IP",
        "FORWARDED_FOR_IP",
        "HTTP_PROXY_CONNECTION",
    ]


class CheckProxy(object):
    # урлы для теста, на которые будем конектится через наши прокси
    # TODO дополнить список
    # list_test_url = (
    #     'http://yahoo.com',
    #     'http://yandex.ru',
    #     'https://mail.ru',
    #     'https://twitter.com',
    #     'https://facebook.com',
    #     'http://stackoverflow.com',
    # )

    list_test_url = (
        'http://yandex.ru',
    )

    validate_type = 'http'  # socks or http

    def run(self, ip, port, user=None, password=None):
        if not port:
            raise ValueError(u'there is no port')
        url = self.get_url(self.list_test_url)
        port = int(port)
        if self.validate_type == 'socks':
            result = self._socks_check_proxy(ip, port, url)
        else:
            result = self._http_check_proxy(ip,
                                            port,
                                            url,
                                            user=user,
                                            password=password)

        return result

    def get_url(self, list_url):
        return choice(list_url)

    def _http_anon_check(self, proxy, verbose=False):
        print("Print anon check: {}".format(proxy))
        personal_ip = '92.127.241.189'

        proxy_host_checker = 'http://www.iprivacytools.com/proxy-checker-anonymity-test/'
        try:
            resp = requests.get(proxy_host_checker, proxies={'http': proxy}, allow_redirects=True, timeout=5.0)
        except (requests.exceptions.SSLError,
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.TooManyRedirects) as e:
            if verbose:
                print(e)
            return False
        else:
            try:
                tree = html.fromstring(resp.text)
                elements = tree.xpath('//li[@class="proxdata"]')

                keys = get_anon_keys()
                if elements:
                    for x in elements:
                        for key in keys:
                            if key in x.text:
                                pass
                                value = x.xpath('.//span')[0].text.strip()
                                if value != 'anonymous / none' and value == personal_ip:
                                    if verbose:
                                        print(key, value)
                                    return False
                else:
                    if verbose:
                        print("Not found html data")
                    return False
            except Exception as e:
                if verbose:
                    print(e)
                return False
        return True

    def _http_check_proxy(self, addr, port, url, user=None, password=None):
        u"""
        :param addr - IP прокси, который будем проверять
        :param port - порт прокси
        :param url - url с помощью которого проверяется валидность прокси
        """
        proxies_dict = {'http': '{}:{}'.format(addr, port)}

        try:
            if user and password:
                _auth = auth.HTTPProxyAuth('username', 'password')
                response = head(url, proxies=proxies_dict, auth=_auth)
            else:
                response = head(url, proxies=proxies_dict)
        except Exception:
            return False

        if response.status_code >= 400:
            return False

        return self._http_anon_check("{}:{}".format(addr, port))

    def _socks_check_proxy(self, addr, port, url, proxtype='http'):
        u"""
        :param addr - IP прокси, который будем проверять
        :param port - порт прокси
        :param url - url с помощью которого проверяется валидность прокси
        :param proxtype - тип прокси, может принимать значение socks или http
        """
        s = socks.socksocket()
        s.setblocking(True)
        s.settimeout(10)

        if proxtype == 'http':
            s.setproxy(socks.PROXY_TYPE_HTTP, addr, port)
        elif proxtype == 'socks':
            s.setproxy(socks.PROXY_TYPE_SOCKS5, addr, port)

        try:
            s.connect((url, 80))
        except Exception:
            return False

        return True


class CheckProxyTask(Task):
    u"""
    Проверка проксей на работоспособность
    """

    def run(self, proxy_id, *args, **kwargs):
        print("Start tasks: CheckProxyTask")
        from backend.models import Proxy

        proxy = Proxy.objects.get(id=proxy_id)
        result = CheckProxy().run(proxy.ip,
                                  proxy.port,
                                  user=proxy.login,
                                  password=proxy.password)
        if not result:
            proxy.delete()


class PeriodicCheckProxyTask(PeriodicTask):
    u"""
    Проверка проксей на работоспособность каждые два часа
    """

    run_every = crontab(minute='*/15')

    def run(self, *args, **kwargs):
        print("Start tasks: PeriodicCheckProxyTask")
        from backend.models import Proxy
        proxies = Proxy.objects.all()
        cp = CheckProxy()
        for proxy in proxies.iterator():
            result = cp.run(proxy.ip,
                            proxy.port,
                            user=proxy.login,
                            password=proxy.password)
            if not result:
                proxy.delete()
