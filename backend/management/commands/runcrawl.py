# coding: utf-8

from os import chdir, getcwd
from os.path import join
from subprocess import PIPE, Popen

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    """
    scrapy crawl post_spider -a id=1 -a do_action=yes
    """
    help = u"starter scrapy crawl"
    path_scrapers = join('backend', 'scrapers')
    path_scrapy_custom = join(path_scrapers, 'scrapycustom', 'scrapycustom')
    path_scrapy_dynamic = join(path_scrapers, 'scrapydynamic', 'scrapydynamic')
    command_custom = 'scrapy crawl {}'
    command_dynamic = 'scrapy crawl {} -a id={} -a do_action=yes'

    def handle(self, *args, **options):
        len_args = len(args)
        if len_args < 1 or len_args > 2:
            raise CommandError(
                'Entered invalid arguments.\n'
                'Enter the name of the spider and id site.\n'
                'Example: python manage.py spider_name or python manage.py spider_name 1, '
                'where 1 == site_id')

        spider_name = args[0]

        if len_args == 2:
            site_id, path_scrapy = args[1], self.path_scrapy_dynamic
            command = self.command_dynamic.format(spider_name, site_id)
        else:
            path_scrapy = self.path_scrapy_custom
            command = self.command_custom.format(spider_name)

        self._run_crawl(command, path_scrapy)

    def _execute_command(self, command):
        process = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        if process.returncode:
            result = [u'command: %s' % command, ]
            if stdout.strip():
                result.append(u'stdout:\n%s' % unicode(stdout.strip(),
                                                       'utf-8'))
            if stderr.strip():
                result.append(u'stderr:\n%s' % unicode(stderr.strip(),
                                                       'utf-8'))
            raise CommandError(u'\n'.join(result))
        print(stdout)
        print(stderr)

    def _run_crawl(self, command, path_scrapy):
        cwd = getcwd()
        chdir(path_scrapy)
        try:
            self._execute_command(command)
        finally:
            chdir(cwd)
