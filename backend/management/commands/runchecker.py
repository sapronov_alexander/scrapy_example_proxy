# coding: utf-8

from django.core.management.base import BaseCommand

from backend.tasks_proxy import PeriodicCheckProxyTask


class Command(BaseCommand):
    """
    scrapy crawl post_spider -a id=1 -a do_action=yes
    """
    help = u"start proxy checker"

    def handle(self, *args, **options):
        PeriodicCheckProxyTask().run()
