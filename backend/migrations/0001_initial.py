# -*- coding: utf-8 -*-
from django.db import models
from south.db import db
from south.utils import datetime_utils as datetime
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    def forwards(self, orm):
        # Adding model 'Site'
        db.create_table(
            u'backend_site',
            ((u'id', self.gf('django.db.models.fields.AutoField')(
                primary_key=True)),
             ('name', self.gf('django.db.models.fields.CharField')(
                 max_length=256)),
             ('url', self.gf('django.db.models.fields.URLField')(
                 max_length=200)),
             ('scraper', self.gf('django.db.models.fields.related.ForeignKey')(
                 to=orm[
                     'dynamic_scraper.Scraper'],
                 null=True,
                 on_delete=models.SET_NULL,
                 blank=True)), ))
        db.send_create_signal(u'backend', ['Site'])

        # Adding model 'Proxy'
        db.create_table(
            u'backend_proxy',
            ((u'id', self.gf('django.db.models.fields.AutoField')(
                primary_key=True)),
             ('ip', self.gf('django.db.models.fields.CharField')(
                 max_length=255)),
             ('port', self.gf('django.db.models.fields.CharField')(
                 max_length=255)),
             ('login',
              self.gf('django.db.models.fields.CharField')(max_length=255,
                                                           null=True,
                                                           blank=True)),
             ('password',
              self.gf('django.db.models.fields.CharField')(max_length=255,
                                                           null=True,
                                                           blank=True)),
             ('country',
              self.gf('django.db.models.fields.CharField')(max_length=255,
                                                           null=True,
                                                           blank=True)),
             ('type',
              self.gf('django.db.models.fields.CharField')(max_length=255,
                                                           null=True,
                                                           blank=True)),
             ('uptime_percent',
              self.gf('django.db.models.fields.CharField')(max_length=255,
                                                           null=True,
                                                           blank=True)),
             ('last_update',
              self.gf('django.db.models.fields.CharField')(max_length=255,
                                                           null=True,
                                                           blank=True)),
             ('url', self.gf('django.db.models.fields.URLField')(
                 max_length=200)),
             ('site', self.gf('django.db.models.fields.related.ForeignKey')(
                 to=orm['backend.Site'])), ))
        db.send_create_signal(u'backend', ['Proxy'])

        # Adding model 'TaskScheduler'
        db.create_table(
            u'backend_taskscheduler',
            ((u'periodictask_ptr',
              self.gf('django.db.models.fields.related.OneToOneField')(
                  to=orm['djcelery.PeriodicTask'],
                  unique=True,
                  primary_key=True)),
             ('repeat', self.gf('django.db.models.fields.BooleanField')(
                 default=False)), ))
        db.send_create_signal(u'backend', ['TaskScheduler'])

    def backwards(self, orm):
        # Deleting model 'Site'
        db.delete_table(u'backend_site')

        # Deleting model 'Proxy'
        db.delete_table(u'backend_proxy')

        # Deleting model 'TaskScheduler'
        db.delete_table(u'backend_taskscheduler')

    models = {
        u'backend.proxy': {
            'Meta': {'object_name': 'Proxy'},
            'country':
            ('django.db.models.fields.CharField', [], {'max_length': '255',
                                                       'null': 'True',
                                                       'blank': 'True'}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip':
            ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'last_update':
            ('django.db.models.fields.CharField', [], {'max_length': '255',
                                                       'null': 'True',
                                                       'blank': 'True'}),
            'login':
            ('django.db.models.fields.CharField', [], {'max_length': '255',
                                                       'null': 'True',
                                                       'blank': 'True'}),
            'password':
            ('django.db.models.fields.CharField', [], {'max_length': '255',
                                                       'null': 'True',
                                                       'blank': 'True'}),
            'port':
            ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'site': ('django.db.models.fields.related.ForeignKey', [],
                     {'to': u"orm['backend.Site']"}),
            'type': ('django.db.models.fields.CharField', [],
                     {'max_length': '255',
                      'null': 'True',
                      'blank': 'True'}),
            'uptime_percent': ('django.db.models.fields.CharField', [],
                               {'max_length': '255',
                                'null': 'True',
                                'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length':
                                                             '200'})
        },
        u'backend.site': {
            'Meta': {'object_name': 'Site'},
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name':
            ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'scraper': ('django.db.models.fields.related.ForeignKey', [],
                        {'to': u"orm['dynamic_scraper.Scraper']",
                         'null': 'True',
                         'on_delete': 'models.SET_NULL',
                         'blank': 'True'}),
            'url':
            ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'backend.taskscheduler': {
            'Meta': {'object_name': 'TaskScheduler',
                     '_ormbases': [u'djcelery.PeriodicTask']},
            u'periodictask_ptr':
            ('django.db.models.fields.related.OneToOneField', [],
             {'to': u"orm['djcelery.PeriodicTask']",
              'unique': 'True',
              'primary_key': 'True'}),
            'repeat':
            ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'djcelery.crontabschedule': {
            'Meta':
            {'ordering':
             "[u'month_of_year', u'day_of_month', u'day_of_week', u'hour', u'minute']",
             'object_name': 'CrontabSchedule'},
            'day_of_month':
            ('django.db.models.fields.CharField', [], {'default': "u'*'",
                                                       'max_length': '64'}),
            'day_of_week':
            ('django.db.models.fields.CharField', [], {'default': "u'*'",
                                                       'max_length': '64'}),
            'hour':
            ('django.db.models.fields.CharField', [], {'default': "u'*'",
                                                       'max_length': '64'}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'minute':
            ('django.db.models.fields.CharField', [], {'default': "u'*'",
                                                       'max_length': '64'}),
            'month_of_year':
            ('django.db.models.fields.CharField', [], {'default': "u'*'",
                                                       'max_length': '64'})
        },
        u'djcelery.intervalschedule': {
            'Meta': {'ordering': "[u'period', u'every']",
                     'object_name': 'IntervalSchedule'},
            'every': ('django.db.models.fields.IntegerField', [], {}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'period':
            ('django.db.models.fields.CharField', [], {'max_length': '24'})
        },
        u'djcelery.periodictask': {
            'Meta': {'object_name': 'PeriodicTask'},
            'args':
            ('django.db.models.fields.TextField', [], {'default': "u'[]'",
                                                       'blank': 'True'}),
            'crontab': ('django.db.models.fields.related.ForeignKey', [],
                        {'to': u"orm['djcelery.CrontabSchedule']",
                         'null': 'True',
                         'blank': 'True'}),
            'date_changed':
            ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True',
                                                           'blank': 'True'}),
            'description':
            ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'enabled':
            ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'exchange':
            ('django.db.models.fields.CharField', [], {'default': 'None',
                                                       'max_length': '200',
                                                       'null': 'True',
                                                       'blank': 'True'}),
            'expires':
            ('django.db.models.fields.DateTimeField', [], {'null': 'True',
                                                           'blank': 'True'}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interval': ('django.db.models.fields.related.ForeignKey', [],
                         {'to': u"orm['djcelery.IntervalSchedule']",
                          'null': 'True',
                          'blank': 'True'}),
            'kwargs': ('django.db.models.fields.TextField', [],
                       {'default': "u'{}'",
                        'blank': 'True'}),
            'last_run_at': ('django.db.models.fields.DateTimeField', [],
                            {'null': 'True',
                             'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [],
                     {'unique': 'True',
                      'max_length': '200'}),
            'queue': ('django.db.models.fields.CharField', [],
                      {'default': 'None',
                       'max_length': '200',
                       'null': 'True',
                       'blank': 'True'}),
            'routing_key': ('django.db.models.fields.CharField', [],
                            {'default': 'None',
                             'max_length': '200',
                             'null': 'True',
                             'blank': 'True'}),
            'task': ('django.db.models.fields.CharField', [], {'max_length':
                                                               '200'}),
            'total_run_count': ('django.db.models.fields.PositiveIntegerField',
                                [], {'default': '0'})
        },
        u'dynamic_scraper.scrapedobjclass': {
            'Meta': {'ordering': "['name']",
                     'object_name': 'ScrapedObjClass'},
            'checker_scheduler_conf':
            ('django.db.models.fields.TextField', [],
             {'default':
              '\'"MIN_TIME": 1440,\\n"MAX_TIME": 10080,\\n"INITIAL_NEXT_ACTION_FACTOR": 1,\\n"ZERO_ACTIONS_FACTOR_CHANGE": 5,\\n"FACTOR_CHANGE_FACTOR": 1.3,\\n\''
              }),
            'comments':
            ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name':
            ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'scraper_scheduler_conf':
            ('django.db.models.fields.TextField', [],
             {'default':
              '\'"MIN_TIME": 15,\\n"MAX_TIME": 10080,\\n"INITIAL_NEXT_ACTION_FACTOR": 10,\\n"ZERO_ACTIONS_FACTOR_CHANGE": 20,\\n"FACTOR_CHANGE_FACTOR": 1.3,\\n\''
              })
        },
        u'dynamic_scraper.scraper': {
            'Meta': {'ordering': "['name', 'scraped_obj_class']",
                     'object_name': 'Scraper'},
            'checker_ref_url':
            ('django.db.models.fields.URLField', [], {'max_length': '500',
                                                      'blank': 'True'}),
            'checker_type':
            ('django.db.models.fields.CharField', [], {'default': "'N'",
                                                       'max_length': '1'}),
            'checker_x_path':
            ('django.db.models.fields.CharField', [], {'max_length': '200',
                                                       'blank': 'True'}),
            'checker_x_path_result':
            ('django.db.models.fields.CharField', [], {'max_length': '200',
                                                       'blank': 'True'}),
            'comments':
            ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'content_type':
            ('django.db.models.fields.CharField', [], {'default': "'H'",
                                                       'max_length': '1'}),
            u'id':
            ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_items_read': ('django.db.models.fields.IntegerField', [],
                               {'null': 'True',
                                'blank': 'True'}),
            'max_items_save': ('django.db.models.fields.IntegerField', [],
                               {'null': 'True',
                                'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length':
                                                               '200'}),
            'pagination_append_str': ('django.db.models.fields.CharField', [],
                                      {'max_length': '200',
                                       'blank': 'True'}),
            'pagination_on_start': ('django.db.models.fields.BooleanField', [],
                                    {'default': 'False'}),
            'pagination_page_replace': ('django.db.models.fields.TextField',
                                        [], {'blank': 'True'}),
            'pagination_type': ('django.db.models.fields.CharField', [],
                                {'default': "'N'",
                                 'max_length': '1'}),
            'scraped_obj_class': (
                'django.db.models.fields.related.ForeignKey', [],
                {'to': u"orm['dynamic_scraper.ScrapedObjClass']"}),
            'status': ('django.db.models.fields.CharField', [],
                       {'default': "'P'",
                        'max_length': '1'})
        }
    }

    complete_apps = ['backend']
