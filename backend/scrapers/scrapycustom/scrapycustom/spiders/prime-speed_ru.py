# coding: utf-8

from ..base.basespider import Spider


class PrimespeedRuSpider(Spider):
    name = 'prime-speed_ru'
    allowed_domains = ['prime-speed.ru']
    start_urls = [
        'http://www.prime-speed.ru/proxy/free-proxy-list/all-working-proxies.php',
    ]

    def parse(self, response):
        return self.process_simple(response, '//div[@class="1"]/pre/text()')
