# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import CrawlSpider


class FoxtoolsRuSpider(CrawlSpider):
    name = 'foxtools_ru'
    allowed_domains = ['foxtools.ru']
    start_urls = ['http://foxtools.ru/Proxy?page=1', ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('\\?page=\\d')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        d = {'ip': 0, 'port': 1, 'country': 2, 'type': 4, }
        return self.process_table(response, 'theProxyList', 'id', d)
