# coding: utf-8

from ..base.basespider import Spider


class FineproxyOrgSpider(Spider):
    name = 'fineproxy_org'
    allowed_domains = ['fineproxy.org']
    start_urls = [
        'http://fineproxy.org/%D1%81%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%BA%D1%81%D0%B8/',
    ]

    def parse(self, response):
        return self.process_simple(response,
                                   '//div[@class="storycontent"]//p/text()')
