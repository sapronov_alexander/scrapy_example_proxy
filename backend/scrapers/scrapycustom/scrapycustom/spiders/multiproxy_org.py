# coding: utf-8

from ..base.basespider import Spider


class MultiproxyORGSpider(Spider):
    name = 'multiproxy_org'
    allowed_domains = ['multiproxy.org']
    start_urls = ['http://multiproxy.org/txt_all/proxy.txt', ]

    def parse(self, response):
        return self.process_simple(response, '//pre/text()')
