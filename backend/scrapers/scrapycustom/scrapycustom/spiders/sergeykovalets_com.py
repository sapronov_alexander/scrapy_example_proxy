# coding: utf-8

from ..base.basespider import Spider


class SergeykovaletsComSpider(Spider):
    name = 'sergeykovalets_com'
    allowed_domains = ['sergeykovalets.com']
    start_urls = [
        'http://sergeykovalets.com/archives/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D1%80%D0%B0%D0%B1%D0%BE%D1%87%D0%B8%D1%85-%D0%BF%D1%80%D0%BE%D0%BA%D1%81%D0%B8-%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%BE%D0%B2/',
    ]

    def parse(self, response):
        return self.process_simple(response,
                                   '//div[@class="entry-content"]//p/text()')
