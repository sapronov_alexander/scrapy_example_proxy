# coding: utf-8

from ..base.basespider import Spider


class StopinfectionNarodRuSpider(Spider):
    name = 'stopinfection_narod_ru'
    allowed_domains = ['stopinfection.narod.ru']
    start_urls = ['http://www.stopinfection.narod.ru/Proxy.htm', ]

    def parse(self, response):
        return self.process_simple(response,
                                   '//p[@class="MsoNormal"]//span/text()')
