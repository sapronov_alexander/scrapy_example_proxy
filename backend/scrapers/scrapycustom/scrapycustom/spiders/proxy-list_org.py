# coding: utf-8

from backend.models import ProxyItem
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import Spider


class FreeproxylistORGSpider(Spider):
    name = 'proxy-list_org'
    allowed_domains = ['proxy-list.org']
    start_urls = ['http://proxy-list.org/ru/index.php?sp=0', ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('\\?sp=\\d')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        # todo
        # спрятать этот обработчик внутрь
        proxies = self.xpath(response, '//tr[@class="%s"]' % 'RegularText')

        proxies = proxies[10:28]  # начало и конец таблицы с проксями
        items = []
        for proxy in proxies:
            item = ProxyItem()
            # 0 - Server ip:port
            # 1 - Type (прозрачный/элитный)
            # 2 - Type (http...)
            # 3 - Uptime in percent
            # 4 - Last_update

            elems = proxy.xpath('.//td/text()')

            item['url'] = response.url
            item['ip'] = self._get_ip(elems, 0)
            item['port'] = self._get_port(elems, 0)
            item['type'] = self._safeget(elems, 2)
            item['uptime_percent'] = self._safeget(elems, 3)
            item['last_update'] = self._safeget(elems, 4)

            item = self._post_filling(item)

            items.append(item)
        return items
