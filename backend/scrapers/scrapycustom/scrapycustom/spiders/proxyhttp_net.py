# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import CrawlSpider


class FreeproxylistORGSpider(CrawlSpider):
    name = 'proxyhttp_net'
    allowed_domains = ['proxyhttp.net']
    start_urls = [
        'http://proxyhttp.net/free-list/anonymous-server-hide-ip-address/1#proxylist',
    ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('/\\d#proxylist')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        # todo
        d = {'ip': 0, 'port': 1, 'country': 3, 'last_update': 6, }
        return self.process_table(response, 'proxytbl', 'class', d)
