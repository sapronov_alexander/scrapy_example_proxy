# coding: utf-8

from ..base.basespider import Spider


class ProxyLifeORGSpider(Spider):
    name = 'proxylife_org'
    allowed_domains = ['proxylife.org']
    start_urls = ['http://proxylife.org/proxy/', ]

    def parse(self, response):
        # todo
        # проблема с портом (опять в js оно)
        # 0 - IP address
        # 1 - Port
        # 2 - Country
        # 3 - Anonymity
        # 4 - HTTPS
        # 5 - Checked (ago)
        d = {
            'ip': 0,
            'port': 0,
            'type': 3,
            'country': 5,
            'last_update': 4,
            'uptime_percent': 6,
        }
        return self.process_table(response, 'style1', 'class', d)
