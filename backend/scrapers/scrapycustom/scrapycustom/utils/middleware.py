# coding: utf-8

from base64 import encodestring
from random import choice, randint

from agents import AGENTS
from backend.models import Proxy
from backend.tasks_proxy import CheckProxy
from scrapy import log


class CustomHttpProxyMiddleware(object):

    attempt = 5

    def process_request(self, request, spider):
        if self.change_proxy():
            proxies = Proxy.objects.all()
            len_proxies = proxies.count()

            cp = CheckProxy()
            result = False
            c = 0
            while not result and len_proxies and c < self.attempt:
                # валидируем прокси
                proxy = self.get_proxy(proxies, len_proxies)
                result = cp.run(proxy.ip,
                                proxy.port,
                                user=proxy.login,
                                password=proxy.password)
                c += 1

            if result:
                proxy_addr = 'http://{}:{}'.format(proxy.ip, proxy.port)
                try:
                    request.meta['proxy'] = proxy_addr
                    if proxy.login and proxy.password:
                        proxy_user_pass = '{u}:{p}'.format(u=proxy.login,
                                                           p=proxy.password)
                        encoded_user_pass = encodestring(proxy_user_pass)
                        request.headers[
                            'Proxy-Authorization'] = 'Basic ' + encoded_user_pass
                except Exception as e:
                    log.msg('Exception %s' % e, _level=log.CRITICAL)

    def get_proxy(self, proxies, len_proxies):
        u"""
        :param proxies: список проксей
        :param len_proxies: количество проксей
        :return: возвращает случайно выбранный прокси
        """
        i = randint(0, len_proxies - 1)
        proxy = proxies[i]
        return proxy

    def change_proxy(self):
        i = randint(1, 10)
        return i <= 2


class CustomUserAgentMiddleware(object):

    default_agent = "'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36'"

    def process_request(self, request, spider):
        if self.change_agent():
            agent = choice(AGENTS)
            request.headers['User-Agent'] = agent

    def change_agent(self):
        i = randint(1, 10)
        return i <= 2
