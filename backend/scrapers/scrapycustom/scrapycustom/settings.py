# -*- coding: utf-8 -*-

# Scrapy settings for scrapyposts project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.dirname(PROJECT_ROOT)))))

os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'project.settings')  # Changed in DDS v.0.3

BOT_NAME = 'scrapycustom'

SPIDER_MODULES = ['scrapycustom.spiders']

# Crawl responsibly by identifying yourself (and your website) on the
# user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36'

ITEM_PIPELINES = ['scrapycustom.pipelines.ProxyItemPipeline', ]

DOWNLOADER_MIDDLEWARES = {
    'scrapycustom.utils.middleware.CustomHttpProxyMiddleware': 400,
    'scrapycustom.utils.middleware.CustomUserAgentMiddleware': 401,
}
