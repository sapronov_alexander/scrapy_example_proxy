# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import CrawlSpider


class ToolsRosinstrumentComSpider(CrawlSpider):
    name = 'site_tools_rosinstrument_com'
    allowed_domains = ['tools.rosinstrument.com']
    start_urls = ['http://tools.rosinstrument.com/raw_free_db.htm?0', ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('\\d')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        # todo
        # доделать (страница не получается, почему-то)
        print(response.body)
        d = {'ip': 1, 'port': 1, 'type': 2, 'last_update': 3, }
        return self.process_table(response, '4', 'cellpadding', d)
