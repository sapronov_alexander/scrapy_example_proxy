# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import Spider


class Proxy4FreeComSpider(Spider):
    name = 'site_proxy4free_com'
    allowed_domains = ['proxy4free.com']
    start_urls = ['http://www.proxy4free.com/list/webproxy1.html', ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('webproxy\\d')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        '''

        При обычном парсинге в таблице стоят "*", чтобы увидеть реальные цифры
        надо регать аккаунт и платить
        :param response:
        :return:
        '''

        d = {'ip': 0, 'port': 1, 'country': 2, 'type': 4, }
        return self.process_table(
            response, 'table.table-bordered.table-striped.proxy-list', 'class',
            d)
