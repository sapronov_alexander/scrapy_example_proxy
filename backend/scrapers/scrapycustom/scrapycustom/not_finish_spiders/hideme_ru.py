# coding: utf-8

from ..base.basespider import CrawlSpider


class HidemeRuSpider(CrawlSpider):
    name = 'site_hideme_ru'
    allowed_domains = ['hideme.org']
    start_urls = ['http://hideme.ru/proxy-list/', ]

    def parse(self, response):
        # todo
        """Порт в виде картинки, надо его распознать.

        :param response:
        :return:

        """

        d = {'ip': 0, 'type': 1, }
        return self.process_table(response, 'pl', 'class', d)
