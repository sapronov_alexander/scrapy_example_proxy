# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import CrawlSpider


class HidemyassComSpider(CrawlSpider):
    name = 'site_hidemyass_com'
    allowed_domains = ['hidemyass.com']

    start_urls = ['http://proxylist.hidemyass.com/1', ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('\\d')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        """Много стилей, около чисел.

        :param response:
        :return:

        """

        # print(response.body)

        d = {'ip': 0, 'port': 1, 'country': 5, 'type': 3, }

        styles = {'id': 'listable', 'class': 'hma-table', }

        return self.process_table_styles(response, styles, d)
