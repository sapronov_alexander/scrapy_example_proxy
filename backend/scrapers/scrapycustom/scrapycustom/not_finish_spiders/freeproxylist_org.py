# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import CrawlSpider


class FreeproxylistORGSpider(CrawlSpider):
    name = 'site_freeproxylist_org'
    allowed_domains = ['freeproxylist.org']
    start_urls = ['http://freeproxylist.org/en/free-proxy-list.htm?index=1', ]

    rules = (Rule(
        SgmlLinkExtractor(allow=('\\?index=\\d')),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        '''

        При обычном парсинге в таблице стоят "*", чтобы увидеть реальные цифры
        надо регать аккаунт и платить
        :param response:
        :return:
        '''

        d = {'ip': 0, 'port': 1, 'country': 2, 'type': 4, }
        return self.process_table(response, 'proxytable', 'id', d)
