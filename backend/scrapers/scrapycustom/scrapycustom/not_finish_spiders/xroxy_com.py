# coding: utf-8

from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

from ..base.basespider import CrawlSpider


class XroxyComSpider(CrawlSpider):
    name = 'site_xroxy_com'
    allowed_domains = ['xroxy.com']

    start_urls = [
        'http://www.xroxy.com/proxylist.php?port=&type=&ssl=&country=&latency=&reliability=&sort=reliability&desc=true&pnum=0',
    ]

    rules = (Rule(
        SgmlLinkExtractor(allow=r'pnum='),
        'parse_item',
        follow=True), )

    def parse_item(self, response):
        """
        :param response:
        :return:
        """

        # print(response.body)

        d = {'ip': 0, 'port': 1, 'country': 5, 'type': 3, }

        element = 'table'

        styles = {'cellpadding': '3', }
        return self.process_table_styles(response, styles, d, 4, element)
