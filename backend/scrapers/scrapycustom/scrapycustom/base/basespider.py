# -*- encoding: utf-8 -*-
import GeoIP
import re

from scrapy import Spider as SSpider
from scrapy.contrib.spiders import CrawlSpider as SCrawlSpider
from scrapy.selector import Selector

from backend.models import ProxyItem


class FillerData(object):
    def __init__(self):
        self.gi = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)

    def get_country_by_ip(self, ip):
        return self.gi.country_name_by_addr(ip)


class BaseSpider(object):
    """Класс для скрытия функций Srapy. Враппер это.

    В классе определяются базовый функционал парсинга. такой как парсинг
    таблиц, получения данных из ячейки таблицы.

    """

    def xpath(self, response, line):
        """Функция, которая выполняет xpath.

        Используется для сокрытия класса Selector
        :param response:
        :param line:
        :return:

        """
        sel = Selector(response)
        return sel.xpath(line)

    def process_table_styles(self,
                             response,
                             styles,
                             dict,
                             cnt=1,
                             element='table',
                             element_after='tr'):
        """Функция для получения элементова из таблицы.

        Используется при парсинге почти всех сайтов где есть таблицы
        :param response:
        :param styles:  словарь, который содержит соответствие  "атрибут": "значение"  (например "class": "name")
        :param dict: словарь соответствия между элементами модели ProxyItem и столбцами из таблицы
        :param cnt: номер начиная с которого надо брать строки (могут быть первые N с мусором)
        :param element: название класса который является таблицей
        :param element_after: название под элемента в таблице
        :return: list proxyitem

        """
        proxies = self._get_rows_from_table_styles(response, styles, cnt,
                                                   element, element_after)
        items = []
        filler = FillerData()
        for proxy in proxies:
            item = self._fill_item(proxy, dict)
            item['url'] = response.url
            if item.get('ip') and item.get('port'):
                items.append(self._post_filling(item, filler))
        return items

    def process_table(self, response, name, type, dict, cnt=1):
        """Упрощенный вариант process_table_styles. Это устаревшая функция.
        Будет заменена на process_table_styles.

        :param response:
        :param name: имя атрибута
        :param type: параметр атрибута
        :param dict: словарь соответствия между элементами модели ProxyItem и столбцами из таблицы
        :param cnt: номер начиная с которого надо брать строки (могут быть первые N с мусором)
        :return: list proxyitem

        """
        proxies = self._get_rows_from_table(response, name, type, cnt)
        items = []
        filler = FillerData()
        for proxy in proxies:
            item = self._fill_item(proxy, dict)
            item['url'] = response.url
            if item.get('ip') and item.get('port'):
                items.append(self._post_filling(item, filler))
        return items

    def _post_filling(self, item, filler=FillerData()):
        '''
        Функция, которая пытается добавить какую-то дополнительную информацию
        Если ее не хватает в данных с сайта
        :param item: objct ProxyItem
        :param filler: объект FillerData
        :return:
        '''
        if item.get('country', None) is None:
            item['country'] = filler.get_country_by_ip(item.get('ip'))

        return item

    def process_simple(self, response, name):
        """Функция для парсинга простых сайтов (когда прокси листы укаазаны
        виде одного списка (аля txt))

        :param response:
        :param name: строка для xpath
        :return:

        """

        def get_ip(line):
            return self._get_ip_port_from_line(line).get('ip', '').strip()

        def get_port(line):
            return self._get_ip_port_from_line(line).get('port', '').strip()

        to_save = []

        proxies = self.xpath(response, name)
        if isinstance(proxies, list):
            if len(proxies) > 1:
                for proxy in proxies:
                    line = proxy.extract().strip()
                    to_save.append(line)
            elif len(proxies) == 1:
                lines = (proxies[0]).extract()
                to_save = lines.split('\n')
        else:
            to_save = response.body.split('\n')

        filler = FillerData()
        items = []
        for line in to_save:
            try:

                item = ProxyItem()
                item['url'] = response.url
                item['ip'] = get_ip(line)
                item['port'] = get_port(line)
                item['country'] = filler.get_country_by_ip(get_ip(line))

                if item['ip'] and item['port']:
                    items.append(item)
            except IndexError:
                pass

        return items

    def _get_rows_from_table(self, response, name, type='class', cnt=1):
        """Получение списка объектов строк. Устаревшая функция. Заменяется на
        _get_rows_from_table_styles.

        :param response:
        :param name: значение атрибута таблицы
        :param type: имя атрибута таблиы (например class)
        :param cnt: номер начиная с которого надо брать строки (могут быть первые N с мусором)
        :return: список объектов селектора

        """
        sel = Selector(response)
        proxies = sel.xpath('//table[@%s="%s"]//tr' % (type, name))
        return proxies[cnt:]

    def _get_rows_from_table_styles(self,
                                    response,
                                    styles,
                                    cnt=1,
                                    element='table',
                                    element_after='tr'):
        """Улучшенная версия функции _get_rows_from_table. Имеет более гибкую
        настройку.

        :param response:
        :param styles:  словарь, который содержит соответствие  "атрибут": "значение"  (например "class": "name")
        :param cnt: номер начиная с которого надо брать строки (могут быть первые N с мусором)
        :param element: название класса который является таблицей
        :param element_after: название под элемента в таблице
        :return:

        """
        sel = Selector(response)
        tst = '//%s[' % element
        for key, value in styles.items():
            if 0 == styles.keys().index(key):
                tst += '@%s="%s"' % (key, value)
            else:
                tst += 'and @%s="%s"' % (key, value)
        tst += ']//%s' % element_after
        proxies = sel.xpath(tst)
        return proxies[cnt:]

    def _get_ip_port_from_line(self, sting):
        """Регулярное выражение для получения разной информации из строки.

        :param sting: входая строка
        :return: словарь с данными

        """
        pattern = '((?P<login>\w+):(?P<password>\w+)@)?(?P<ip>\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(:(?P<port>\d+))?'
        t_ob = re.match(pattern, sting)
        if t_ob is not None:
            return t_ob.groupdict()
        else:
            return {}

    def _get_key(self, key, l, idx, default=''):
        """Враппер функциия над _get_ip_port_from_line.

        :param key: имя ключа из функции _get_ip_port_from_line
        :param l: словарь
        :param idx:
        :param default:
        :return:

        """
        return self._get_ip_port_from_line(self._safeget(l, idx)).get(key,
                                                                      default)

    def _get_ip(self, l, idx, default=''):
        """Получение IP из элемента листа.

        :param l:
        :param idx:
        :param default:
        :return:

        """
        return self._get_key('ip', l, idx, default)

    def _get_port(self, l, idx, default=''):
        """Получение порта.

        :param l:
        :param idx:
        :param default:
        :return:

        """
        return self._get_key('port', l, idx, default)

    def _safeget(self, l, idx, default=''):
        """Безопастный способ достать текст из объекта селектора.

        :param l:
        :param idx:
        :param default:
        :return:

        """
        try:
            return self._fromlist(l, idx).extract()
        except Exception as e:
            # print(e)
            return default

    def _fromlist(self, l, idx, default=''):
        """Безопастный get из списка.

        :param l:
        :param idx:
        :param default:
        :return:

        """
        try:
            return l[idx]
        except IndexError:
            return default

    def _fill_item(self, data, keys):
        """Заполнение объекта ProxyItem.

        :param data: объект селектора содержащий строку tr
        :param keys: словарь соответствия между элементами модели ProxyItem и столбцами из таблицы
        :return:

        """
        item = ProxyItem()
        for key, idx in keys.items():
            va = self._must_get(data, idx)

            if key == 'ip' or key == 'port':
                value = self._get_ip_port_from_line(va).get(key, None)
                # print(va, key, value, self._get_ip_port_from_line(va).keys())
                if value is not None:
                    item[key] = value
                else:
                    # костыль
                    item[key] = va
            else:
                item[key] = va

        item = self._post_filling(item)
        return item

    def _get_from_re(self, line, regexp, default=''):
        """Не используется.

        :param line:
        :param regexp:
        :param default:
        :return:

        """
        re_obj = re.compile(regexp)
        match_obj = re_obj.match(line)
        if match_obj is not None:
            return re_obj.findall(line)[0]
        return default

    def _safeextract(self, ob, line):
        """Безопастный extract.

        :param ob:
        :param line:
        :return:

        """
        return self._safeget(ob.xpath(line), 0)

    def _must_get(self, data, idx, default=''):
        """Получение значения из ячейки td.

        :param data: элемент td
        :param idx: индекс колонки
        :param default: значение по умолчанию
        :return:

        """
        tdob = self._fromlist(data.xpath('.//td'), idx)
        if tdob:
            list_actions = [
                self._safeget(data.xpath('.//td/text()'), idx).strip(),
                self._safeextract(tdob, './/script/text()'),
                self._safeextract(tdob, './/img/text()'),
                self._safeextract(tdob, './/a/text()'),
            ]
            for value in list_actions:
                if value.strip():
                    return value.strip()
        return default


class Spider(SSpider, BaseSpider):
    pass


class CrawlSpider(SCrawlSpider, BaseSpider):
    pass
