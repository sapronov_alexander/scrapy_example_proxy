# -*- coding: utf-8 -*-

# Scrapy settings for scrapyposts project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(
    os.path.dirname(PROJECT_ROOT)))))

os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'project.settings')  # Changed in DDS v.0.3

BOT_NAME = 'scrapydynamic'

SPIDER_MODULES = ['dynamic_scraper.spiders', 'scrapydynamic.spiders']
# NEWSPIDER_MODULE = 'scrapyposts.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'scrapyposts (+http://www.yourdomain.com)'

USER_AGENT = '%s/%s' % (BOT_NAME, '1.0')

ITEM_PIPELINES = [
    'dynamic_scraper.pipelines.ValidationPipeline',
    'scrapydynamic.pipelines.ScrapyProxyPipeline',
]
