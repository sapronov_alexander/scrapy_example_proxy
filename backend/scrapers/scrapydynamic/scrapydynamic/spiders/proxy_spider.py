# coding: utf-8

from backend.models import Proxy, ProxyItem, Site
from dynamic_scraper.spiders.django_spider import DjangoSpider


class ProxySpider(DjangoSpider):
    def __init__(self, *args, **kwargs):
        self._set_ref_object(Site, **kwargs)
        self.scraper = self.ref_object.scraper
        self.scrape_url = self.ref_object.url
        self.scraped_obj_class = Proxy
        self.scraped_obj_item_class = ProxyItem
        super(ProxySpider, self).__init__(self, *args, **kwargs)
