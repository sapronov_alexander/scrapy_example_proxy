# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from django.db.utils import IntegrityError
from scrapy import log
from scrapy.exceptions import DropItem


class ScrapyProxyPipeline(object):
    def process_item(self, item, spider):
        try:
            item['site'] = spider.ref_object
            item.save()
            spider.action_successful = True
            spider.log('Item saved.', log.INFO)

        except IntegrityError as e:
            spider.log(str(e), log.ERROR)
            raise DropItem('Missing attribute.')

        return item
