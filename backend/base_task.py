# coding: utf-8

from celery import Task
from django.core import management
from models import Proxy, TaskScheduler


class BaseTask(Task):

    abstract = True
    autoregister = True

    spider_name = None
    site_url = None
    site_id = None

    def run_spider(self,
                   tasksheduler_name,
                   task_id,
                   task_class,
                   spider_name,
                   site_id=None,
                   site_url=None):
        u"""
        Метод запускает паука spider_name для сайта с id==site_id
        :param tasksheduler_name: название расписания
        :param task_id: id таски
        :param task_class: название класса таски
        :param spider_name: название паука
        :param site_id: id сайта в базе таблицы Site
        :param site_url: адрес сайта, который будет парсится
        """

        print('run task: {}'.format(task_id))
        if site_id is not None:
            Proxy.objects.filter(site_id=site_id).delete()
            management.call_command('runcrawl', spider_name, site_id)
        if site_url is not None:
            Proxy.objects.filter(url__contains=site_url).delete()
            management.call_command('runcrawl', spider_name)

        ts = TaskScheduler.objects.get(
            name=tasksheduler_name,
            task='backend.tasks_spiders.' + task_class)
        if not ts.repeat:
            print('stop task: {}'.format(task_id))
            ts.terminate()

    def validate_site_data(self):
        if not self.site_url and not self.site_id:
            raise ValueError(u'must be declared site_url or site_id!')

    def run(self, *args, **kwargs):
        tasksheduler_name = kwargs['name']
        task_id = self.request.id
        task_class = str(self.__class__.__name__)

        print('task_class: {}'.format(task_class))

        self.validate_site_data()
        self.run_spider(tasksheduler_name,
                        task_id,
                        task_class,
                        self.spider_name,
                        site_url=self.site_url,
                        site_id=self.site_id)
