# -*- encoding: utf-8 -*-

from django.conf.urls import patterns, url
from frontend.views import IndexPage

urlpatterns = patterns('', url(r'^$', IndexPage.as_view(), name='index'), )
